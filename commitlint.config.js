module.exports = {
  extends: ['@commitlint/config-conventional'],
  parserPreset: {
    parserOpts: {
      headerPattern: /^(\w+): (.*) (?:\[#(\d+)\])?$/,
      headerCorrespondence: ['type', 'subject', 'taskID'],
    },
  },
  rules: {
    'references-empty': [2, 'never'],
    'type-enum': [2, 'always', ['feat', 'fix', 'chore', 'docs', 'ci']],
  },
}
