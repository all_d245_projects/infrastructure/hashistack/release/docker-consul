#> common
variable "docker_login_server" {
  type        = string
  description = "Docker registry server"
  default     = "registry.gitlab.com"
}
variable "docker_login_username" {
  type        = string
  description = "Docker registry login username"
  default     = "hashistack-docker-registry"
}
variable "docker_login_password" {
  type        = string
  sensitive   = true
  description = "Docker registry login password"
}
variable "packer_version" {
  type        = string
  description = "Packer version to install in docker"
  default     = "1.10.0"
}
variable "consul_version" {
  type        = string
  description = "Consul version to install in docker"
  default     = "1.17.1"
}
variable "ansible_version" {
  type        = string
  description = "Ansible version to install in docker"
  default     = "9.1.0"
}
variable "docker_tag_repository" {
  type        = string
  description = "Docker tag repository"
  default     = "registry.gitlab.com/all_d245_projects/infrastructure/hashistack/release/docker-consul"
}
variable "docker_tag_tags" {
  type        = set(string)
  description = "Docker tag array of tags"
  default     = ["latest"]
}
variable "one_password_service_account_token" {
  type        = string
  description = "1password service account token" 
  sensitive   = true
}
variable "one_password_cli_version" {
  type        = string
  description = "1password cli version" 
  default     = "2.24.0"
}
#<