source "docker" "python-base" {
  image       = "python:3.11"
  author      = "asaltson@gmail.com"
  commit      = true
  changes     = [
    "ENTRYPOINT [\"/entrypoint.sh\"]",
    "ENV PACKER_PLUGIN_PATH=\"/project_root/files/packer/cache/.packer_plugins\"",
    "ENV PACKER_CACHE_DIR=\"/project_root/files/packer/cache/.packer_cache\""
  ]
}