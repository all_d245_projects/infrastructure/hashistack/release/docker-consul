packer {
  required_plugins {
    docker = {
      source  = "github.com/hashicorp/docker"
      version = "~> 1"
    }
    ansible = {
      source  = "github.com/hashicorp/ansible"
      version = "~> 1"
    }
  }
}

build {
  name        = "docker-consul"
  description = "Custom docker image to be used for consul builds."
  sources     = ["docker.python-base"]
  source "docker.python-base" {
    name        = "prod"
    message     = "docker-consul"
    run_command = [ "-d", "-i", "-t", "--name=docker-consul", "--entrypoint=/bin/bash", "--", "{{.Image}}" ]
  }
  source "docker.python-base" {
    name        = "local"
    message     = "docker-consul"
    run_command = [ "-d", "-i", "-t", "--name=docker-consul", "--entrypoint=/bin/bash", "--", "{{.Image}}" ]
  }

  provisioner "file" {
    source      = "${path.root}/files/scripts/docker-consul-entrypoint.sh"
    destination = "/entrypoint.sh"
  }

  provisioner "shell" {
    inline = ["chmod +x /entrypoint.sh"]
  }

  provisioner "shell" {
    inline = [
      "apt-get update",
      "apt-get install --no-install-recommends -y python3.11 python3-dev python3-pip libffi-dev libssl-dev build-essential",
      "pip3 install ansible==${var.ansible_version} netaddr"
    ]
  }

  provisioner "ansible" {
    playbook_file    = "${path.root}/../../ansible/playbooks/docker-consul/playbook.yml"
    user             = "root"
    extra_arguments  = [
      # ansible vars
      "-e ansible_host=docker-consul",
      "-e ansible_connection=docker",

      # custom vars
      "-e packer_vars_json=${jsonencode(var)}",
    ]
  }

  post-processors {
    post-processor "docker-tag" {
      repository = var.docker_tag_repository
      tags       = var.docker_tag_tags
    }
    post-processor "docker-push" {
      except         = ["docker.local"]
      login          = true
      login_server   = var.docker_login_server
      login_username = var.docker_login_username
      login_password = var.docker_login_password
    }
  }
}