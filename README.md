# Docker Consul


This repository is responsible for pulling changes **directly related** to docker-consul from the [upstream repository](https://gitlab.com/all_d245_projects/infrastructure/hashistack/docker-images), as well as deploying a release.

## Git Commit
The project adheres to the commit message [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#summary)
The structure used is
```
<type>: <issue title> [<issue-code>]
Closes <issue code>
```

An example commit would be:
```
feat: implement docker image build [#21]
Closes #21
```

The supported `types` for this project are defined in `commitlint.config.js`.

This commit structure is enforced as part of the pre-commit hook configured via husky, as well as in the pipelines.

## Husky (git hooks)
Husky is used to register some actions during some git hooks. At the moment, the following git hooks are registered:
- commit-msg: Validate commit messages against `commitlint.config.js`

## Pipeline
This repository essentially has two pipeline roles:

1. Pull changes from the upstream repository and commit them to the `main` branch

2. Generate a release once a tag has been created 
